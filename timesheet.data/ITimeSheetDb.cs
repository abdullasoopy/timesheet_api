﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using timesheet.model;

namespace timesheet.data
{
    public interface ITimeSheetDb
    {

        DbSet<Employee> Employees { get; set; }
        DbSet<Task> Tasks { get; set; }
    }
}
