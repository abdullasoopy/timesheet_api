﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace timesheet.model
{
    public class TimeSheet
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }

        [Required]
        public int EmployeeID { get; set; }

       // [Required]
        public int Week { get; set; }

        [Required]
        public int Day { get; set; }

        [Required]
        public int TaskID { get; set; }

        [Required]
        public int HoursWorked { get; set; }
    }
}
