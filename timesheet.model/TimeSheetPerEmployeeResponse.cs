﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class TimeSheetPerEmployeeResponse
    {

        public string Task { get; set; }

        public int TaskID { get; set; }

        public int Hours { get; set; }

        public int Day { get; set; }
    }


    public class EmplyeeStatistics
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public int TotalEffort { get; set; }

        public double AverageEffort { get; set; }
    }
}
