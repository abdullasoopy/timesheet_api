﻿using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<Employee> GetEmployees()
        {
            return this.db.Employees;
        }

        public List<EmplyeeStatistics> GetEmployeeStatistics()
        {
            try
            {
                var model = this.db.Employees.Select(x => new EmplyeeStatistics
                {
                    Id = x.Id,
                    Code = x.Code,
                    Name = x.Name,
                    TotalEffort = this.db.TimeSheet.Where(t => t.EmployeeID == x.Id).Sum(s => s.HoursWorked),
                    AverageEffort = this.db.TimeSheet.Where(t => t.EmployeeID == x.Id).Count() > 0 ? (this.db.TimeSheet.Where(t => t.EmployeeID == x.Id).Sum(s => s.HoursWorked) /7): 0
                }).ToList();

                return model;
            }
            catch (Exception ex)
            {
                return new List<EmplyeeStatistics>();
            }
        }

        public IQueryable<Task> GetTasks()
        {
            return this.db.Tasks;
        }


        public List<TimeSheetPerEmployeeResponse> GetTimeSheets(int EmployeeID)
        {
            var items = this.db.TimeSheet
                        .Join(this.db.Tasks, x => x.TaskID, e => e.Id, (x, e) => new { X = x, E = e })
                        .Where(e => e.X.EmployeeID == EmployeeID)
                        .Select(w => new TimeSheetPerEmployeeResponse()
                        {
                            Task = w.E.Name,
                            Day = w.X.Day,
                            Hours = w.X.HoursWorked,
                            TaskID = w.X.TaskID
                        }).OrderBy(e => e.TaskID).ToList();
            return items;
        }




        public bool AddTimeSheet(TimeSheet model)
        {
            try
            {
                if (db.TimeSheet.Where(x => x.EmployeeID == model.EmployeeID && x.Day == model.Day && x.Week == model.Week && x.TaskID==model.TaskID).Count() > 0)
                {
                    var upItem = db.TimeSheet.Where(x => x.EmployeeID == model.EmployeeID && x.Day == model.Day && x.Week == model.Week && x.TaskID == model.TaskID).FirstOrDefault();
                    upItem.HoursWorked = model.HoursWorked;

                    db.TimeSheet.Update(upItem);
                    db.SaveChanges();
                }
                else
                {
                    db.TimeSheet.Add(model);

                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


    }
}
