﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeService employeeService;
        public EmployeeController(EmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        [HttpGet("getall")]
        public IActionResult GetAll()
        {
            var items = this.employeeService.GetEmployees();
            return new ObjectResult(items);
        }

        [HttpGet("getallEmpStatistics")]
        public IActionResult getallEmpStatistics()
        {
            var items = this.employeeService.GetEmployeeStatistics();
            return new ObjectResult(items);
        }

        [HttpGet("getalltasks")]
        public IActionResult GetAlltasks()
        {
            var items = this.employeeService.GetTasks();
            return new ObjectResult(items);
        }

        [HttpGet("getallEmployeeTaskSheet")]
        public IActionResult getallEmployeeTaskSheet(int EID)
        {
            var items = this.employeeService.GetTimeSheets(EID);
            return new ObjectResult(items);
        }

        [HttpPost("addtimesheet")]
        public IActionResult AddTimeSheet(TimeSheet model)
        {
            model.Week = GetWeekNumber();
            return new ObjectResult(this.employeeService.AddTimeSheet(model));

        }

        public int GetWeekNumber()
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday);
            return weekNum;
        }
    }
}